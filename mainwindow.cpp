#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(new QGraphicsScene(QRect(0,0,800,400)));
    ui->HrLineEdit->setText(QString::fromStdString(std::to_string(int(hr))));
    ui->HtLineEdit->setText(QString::fromStdString(std::to_string(int(ht))));
    ui->DLineEdit->setText(QString::fromStdString(std::to_string(int(d))));
    ui->FLineEdit->setText(QString::fromStdString(std::to_string(int(f))));
    ui->D0LineEdit->setText(QString::fromStdString(std::to_string(int(d0))));
    ui->E0LineEdit->setText(QString::fromStdString(std::to_string(int(e0))));
    draw();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_HtLineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, ht);
}

void MainWindow::on_HrLineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, hr);
}

void MainWindow::on_DLineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, d);
}

void MainWindow::on_FLineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, f);
}

void MainWindow::on_D0LineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, d0);
}

void MainWindow::on_E0LineEdit_textChanged(const QString &arg1)
{
    textChanged(arg1, e0);
}

void MainWindow::draw()
{
    QGraphicsScene* s = this->ui->graphicsView->scene();
    s->clear();
    double width = s->width();
    double height = s->height();
    double maxHeight = 0.8*height;
    double maxWidth = 0.8*width;
    double wMargin = 0.1*width;
    double hMargin = 0.1*height;
    double lHt = ht / std::max(ht, hr) * maxHeight;
    double lHr = hr / std::max(ht, hr) * maxHeight;
    qDebug() << lHt;
    qDebug() << lHr;
    qDebug() << maxHeight << " " << maxWidth;
    QPen pen;
    pen.setWidth(2);
    s->addLine(0, height - hMargin, width, height - hMargin, pen);

    pen.setColor(Qt::GlobalColor::red);
    s->addLine(wMargin, height - hMargin, wMargin, hMargin + maxHeight - lHt, pen);

    pen.setColor(Qt::GlobalColor::blue);
    s->addLine(width - wMargin, height - hMargin, width - wMargin, hMargin + maxHeight - lHr, pen);

    pen.setColor(Qt::GlobalColor::yellow);
    s->addLine(width - wMargin, hMargin + maxHeight - lHr, wMargin, hMargin + maxHeight - lHt, pen);

    double reflectionPointX = wMargin + (lHt * maxWidth / (lHt + lHr));
    pen.setColor(Qt::GlobalColor::green);
    s->addLine(wMargin, hMargin + maxHeight - lHt, reflectionPointX, height - hMargin, pen);
    pen.setColor(Qt::GlobalColor::darkGreen);
    s->addLine(width - wMargin, hMargin + maxHeight - lHr, reflectionPointX, height - hMargin, pen);

    double pointSize = 5;
    pen.setColor(Qt::GlobalColor::red);
    s->addEllipse(wMargin - pointSize/2, hMargin + maxHeight - lHt - pointSize/2, pointSize, pointSize, pen, Qt::GlobalColor::red);
    pen.setColor(Qt::GlobalColor::blue);
    s->addEllipse(width - wMargin - pointSize/2, hMargin + maxHeight - lHr - pointSize/2, pointSize, pointSize, pen, Qt::GlobalColor::blue);
    double reflectionAngle = 360 * atan2(lHt, reflectionPointX - wMargin) / (2*M_PI);
    double angleSize = 100;
    s->addEllipse(reflectionPointX - angleSize / 2, height - hMargin - angleSize / 2, angleSize, angleSize)->setSpanAngle(reflectionAngle*16);
    auto leftEllipse = s->addEllipse(reflectionPointX - angleSize / 2, height - hMargin - angleSize / 2, angleSize, angleSize);
    leftEllipse->setSpanAngle(-reflectionAngle*16);
    leftEllipse->setStartAngle(-180*16);

    s->addItem(new QGraphicsTextItem("ϴ = " + QString::fromStdString(std::to_string(reflectionAngle)) + "°"));

    auto reflectionAngleText1 = new QGraphicsTextItem("ϴ");
    reflectionAngleText1->setPos(reflectionPointX + angleSize / 5, height - hMargin - angleSize / 5);
    s->addItem(reflectionAngleText1);
    auto reflectionAngleText2 = new QGraphicsTextItem("ϴ");
    reflectionAngleText2->setPos(reflectionPointX - angleSize / 3, height - hMargin - angleSize / 5);
    s->addItem(reflectionAngleText2);

    auto transmiterHeightText = new QGraphicsTextItem(QString::fromStdString(std::to_string(ht)));
    transmiterHeightText->setPos(0.8*wMargin, height - hMargin - (lHt/4));
    transmiterHeightText->setRotation(-90);
    s->addItem(transmiterHeightText);

    auto receiverHeightText = new QGraphicsTextItem(QString::fromStdString(std::to_string(hr)));
    receiverHeightText->setPos(width - 0.8*wMargin, height - hMargin - (3*lHr/4));
    receiverHeightText->setRotation(90);
    s->addItem(receiverHeightText);

    auto distanceText = new QGraphicsTextItem(QString::fromStdString(std::to_string(d)));
    distanceText->setPos((width / 2) - wMargin, height - hMargin);
    s->addItem(distanceText);

    double c = 300000000;

    double Etot = 2*e0*(d0/d)*sin(2*M_PI*f*2*ht*hr/(c*d));
    auto etotText = new QGraphicsTextItem("ETOT = " + QString::fromStdString(std::to_string(Etot)) + " = " + QString::fromStdString(std::to_string(10*log10(Etot/e0))) + "dB");
    etotText->setPos(width/2,0);
    s->addItem(etotText);

}

void MainWindow::textChanged(const QString &arg1, double &value)
{
    bool ok = true;
    double v = arg1.toDouble(&ok);
    if(ok) {
        value = v;
        draw();
    }
}



