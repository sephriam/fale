#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_HtLineEdit_textChanged(const QString &arg1);
    void on_HrLineEdit_textChanged(const QString &arg1);
    void on_DLineEdit_textChanged(const QString &arg1);
    void on_FLineEdit_textChanged(const QString &arg1);
    void on_D0LineEdit_textChanged(const QString &arg1);
    void on_E0LineEdit_textChanged(const QString &arg1);

private:
    void draw();
    void textChanged(const QString &arg1, double& value);

    double ht = 20;
    double hr = 10;
    double d = 100;
    double f = 1000;
    double e0 = 100;
    double d0 = 1;

    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
